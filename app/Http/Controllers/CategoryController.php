<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Category Listing page
     *
     * @return void
     */
    public function index()
    {
        return view('Category.index', [
            'categories' => Category::all() 
        ]);
    }
    
    /**
     * Category edit page
     *
     * @return void
     */
    public function edit($id)
    {
        return view('Category.index', [
            'category'  => Category::find($id),
            'categories' => Category::all() 
        ]);
    }
    


    /**
     * Store new category
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string']
        ]);

        Category::create($validated);

        return redirect()->back()->with(['message' => 'Category has been created!']);
    }

    
    /**
     * Update a category
     *
     * @param [type] $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string']
        ]);

        Category::find($id)->update($validated);

        return redirect()->back()->with(['message' => 'Category has been updated!']);
    }

    
    /**
     * Delete a category
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        Category::find($id)->delete();
        return redirect()->back()->with(['message' => 'Category has been deleted!']);
    }

}

