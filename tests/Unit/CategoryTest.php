<?php

use App\Models\Category;
use Tests\TestCase;

uses(TestCase::class);

it('Category page can be rendered', function () {

    $this->get('/categories')->assertStatus(200);
    
});

it('Category edit page can be rendered', function () {

    $category = Category::factory()->create();
    
    $this->get("/categories/$category->id")->assertStatus(200);
});



it('Category can be inserted in DB', function (){

    $attributes = Category::factory()->raw();
    
    $this->post('/categories', $attributes);

    $this->assertDatabaseHas('categories', $attributes);

});

it('Category can be updated', function (){

    $category = Category::factory()->create();
    
    $this->put("/categories/$category->id", ['name' => 'Test Update']);

    $this->assertDatabaseHas('categories', [
        'id'   => $category->id,
        'name' => 'Test Update'
    ]);
});


it('Category can be deleted', function() {

    $category = Category::factory()->create();
     
    $this->delete("/categories/$category->id");

    $this->assertDatabaseCount('categories', 0);
});