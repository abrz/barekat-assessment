<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Assessment</title>
        @vite('resources/css/app.css')
    </head>
    <body class="bg-slate-800">
            <div class="max-w-7xl mx-auto p-6 lg:p-8">
                
                <div class="text-2xl text-white mb-2">
                    @yield('head')
                    
                    <hr class="mt-2">
                </div>
                <x-errors></x-errors>
                {{ $slot }}
            </div>
    </body>
</html>
