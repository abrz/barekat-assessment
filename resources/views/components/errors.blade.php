    
@if ($errors->any())
    <div class="alert alert-danger border border-red-400 p-2 rounded-sm	">
        <ul>
            @foreach ($errors->all() as $error)
                <li class="text-red-400">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif