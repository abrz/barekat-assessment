<x-layout>


    @if (request()->routeIs('category.index'))
        @section('head')
            <h1 class="align-center">Cateogires</h1>
        @endsection
        <form class="w-full " action="{{ route('category.store') }}" method="POST">
            @csrf
            <div class="flex items-center border-b border-teal-500 py-2">
                <input name="name"
                    class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none focus:text-sky-400"
                    type="text" placeholder="Category Name" aria-label="Category Name">
                <button
                    class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700  text-sm border-4 text-white py-1 px-2 rounded"
                    type="submit">
                    Create
                </button>
            </div>
        </form>
    @endif

    @if (request()->routeIs('category.edit'))
        @section('head')
            <div>
                <a class="text-right text-blue-300 text-sm" href="{{ url()->previous() }}">Back</a>
            </div>
            <h1 class="align-center">Edit #{{ $category->id }}</h1>
        @endsection
        <form class="w-full " action="{{ route('category.update', $category->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="flex items-center border-b border-teal-500 py-2">
                <input name="name"
                    class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none text-slate-300 focus:text-sky-400"
                    type="text" placeholder="Category Name" aria-label="Category Name"
                    value="{{$category->name}}">
                <button
                    class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700  text-sm border-4  py-1 px-2 rounded"
                    type="submit">
                    Update
                </button>
            </div>
        </form>
    @endif

    <table class="border-collapse border border-slate-500 text-white w-full mt-5">
        <thead>
            <tr class="p-1">
                <th class="border border-slate-600">Id</th>
                <th class="border border-slate-600">Name</th>
                <th class="border border-slate-600">Action</th>
            </tr>
        </thead>
        <tbody>
            @if($categories->count())
            @foreach ($categories as $item)
            <tr>
                <td class="border border-slate-600 p-2 text-center">{{ $item->id }}</td>
                <td class="border border-slate-600 p-2 text-center">{{ $item->name }}</td>
                <td class="border border-slate-600 p-2 text-center flex flex-row">
                    <form action="{{ route('category.delete', $item->id) }}" method="POST"
                        id="delete-cat-{{$item->id}}">
                        @csrf
                        @method('delete')
                        <a href="#" onclick="document.getElementById('delete-cat-{{$item->id}}').submit();"
                            class="text-red-400 ">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6 m-auto">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                            </svg>
                        </a>
                    </form>


                    <a href="{{ route('category.edit', $item->id) }}"
                        class="text-blue-300 pr-2 pl-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
                        </svg>
                    </a>

                </td>
            </tr>
            @endforeach
            @else
            <tr class="text-red-600 text-center">
                <td colspan="3" class="p-5 text-xl"> No Record </td>
            </tr>
            @endif
        </tbody>
    </table>
    ​

</x-layout>
